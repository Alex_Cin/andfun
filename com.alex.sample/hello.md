alex{
   skill: android
   age: 27
}
java{
    version:1.8
}
android{
   language:java, kotlin
}
cross_platform{
    flutter{
        provider: google
        language: dart
    }
}